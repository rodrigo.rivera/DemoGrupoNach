package com.example.demo.demo.Entity;


import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
public class Genders {

    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 256)
    private String name;

    @OneToMany(mappedBy = "gender")
    private Set<Employees> genderEmployeess;


}
