package com.example.demo.demo.Entity;


import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.Date;


@Entity
@Data
public class EmployeeEorkedHours {

    @Id
    @Column(nullable = false, updatable = false)

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private Integer workedHours;

    @Column
    private Date workedDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id")
    private Employees employee;


}
