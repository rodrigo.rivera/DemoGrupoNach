package com.example.demo.demo.Entity;


import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
public class Jobs {

    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 256)
    private String name;

    @Column
    private Integer salary;

    @OneToMany(mappedBy = "job")
    private Set<Employees> jobEmployeess;


}
