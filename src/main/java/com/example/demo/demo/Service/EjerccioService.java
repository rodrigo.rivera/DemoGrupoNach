package com.example.demo.demo.Service;

import com.example.demo.demo.Request.*;
import org.springframework.http.ResponseEntity;

public interface EjerccioService {

    ResponseEntity<?> ejerccio01(Ejerccio01Request req);

    ResponseEntity<?> ejerccio02(Ejerccio02Request req);
    ResponseEntity<?> ejerccio03(Ejerccio03Request req);
    ResponseEntity<?> ejerccio04(Ejerccio04Request req);
    ResponseEntity<?> ejerccio05(Ejerccio05Request req);
}
