package com.example.demo.demo.Service.Impl;

import com.example.demo.demo.Entity.Employees;
import com.example.demo.demo.Entity.Genders;
import com.example.demo.demo.Entity.Jobs;
import com.example.demo.demo.Repository.EmployeHoursRepository;
import com.example.demo.demo.Repository.EmployeRepository;
import com.example.demo.demo.Repository.GendersRepository;
import com.example.demo.demo.Repository.JobsRepository;
import com.example.demo.demo.Request.*;
import com.example.demo.demo.Response.*;
import com.example.demo.demo.Service.EjerccioService;
import com.example.demo.demo.Validate.Validations;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Service
public class EjerccioServiceImpl implements EjerccioService {

    @Autowired
    private EmployeRepository employeRepository;

    @Autowired
    private JobsRepository jobsRepository;

    @Autowired
    private GendersRepository gendersRepository;

    @Autowired
    private EmployeHoursRepository employeHoursRepository;

    @Autowired
    private Validations validations;

    @Autowired
    private ParserServiceImp parserServiceImp;


    @Value("${hilos.cantidad}")
    private Integer numHilos;

    ArrayList<Ejerrcio03EmployeeResponse> lista = new ArrayList<>();


    @Override
    public ResponseEntity<?> ejerccio01(Ejerccio01Request req) {

        Ejerccio01Response response;
        Genders gen = gendersRepository.findById(req.getGender_id()).orElse(null);
        Jobs job = jobsRepository.findById(req.getJob_id()).orElse(null);
        Integer existName = employeRepository.procedureName(req.getName(), req.getLast_name());
        boolean validartMayor = validations.validartMayor(req.getBirthdate());
        if (gen == null || job == null || !validartMayor || existName > 0) {
            response = new Ejerccio01Response(null, false);
            return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
        }
        Employees e = new Employees();
        e.setName(req.getName());
        e.setLastName(req.getLast_name());
        e.setJob(job);
        e.setGender(gen);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        try {

            e.setBrithdate(sdf.parse(req.getBirthdate()));
            e = employeRepository.save(e);

        } catch (Exception ee) {
            ee.printStackTrace();
            response = new Ejerccio01Response(null, false);
            return new ResponseEntity<Object>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response = new Ejerccio01Response(e.getId(), true);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> ejerccio02(Ejerccio02Request req) {

        //List<Employees> empleados;
        List<Ejerccio02ResponseEmploye> temp;
        Ejerccio02Response response;
        try {

            List<Integer> empleados = employeRepository.getEmpleados(req.getJob_id());
            parserServiceImp.parseEmplyee(null,empleados);

        } catch (Exception e) {
            e.printStackTrace();
            response = new Ejerccio02Response(null, false);
            return new ResponseEntity<Object>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }

        response = new Ejerccio02Response(null, true);
        System.out.println("salida " + response);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }


    @Override
    public ResponseEntity<?> ejerccio03(Ejerccio03Request req) {

        Employees empleado = employeRepository.findById(3).orElse(null);
        Ejerccio03Response response = new Ejerccio03Response();
        ExecutorService executor = Executors.newFixedThreadPool(numHilos);

        ArrayList<Ejerrcio03EmployeeResponse> list = new ArrayList<>();

        try {

            for (Integer a : req.getEmployee_id()) {

                Search temp = new Search(a, list);
                Runnable worker = temp;
                executor.execute(worker);

            }
            executor.shutdown();
            while (!executor.isTerminated()) {
            }
        } catch (Exception e) {
            response.setSuccess(false);
            response.setEmployees(null);
            return new ResponseEntity<Object>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }


        response.setSuccess(true);
        response.setEmployees(list);
        response.setEmployees(list);
        System.out.println("hola:" + list);
        System.out.println("hola:" + list);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> ejerccio04(Ejerccio04Request req) {
        Ejerccio04Response response;
        Integer horas = null;
        boolean validateDate = validations.validarFecha(req.getStart_date(), req.getEnd_date());
        Employees tempEmploye = employeRepository.findById(req.getEmployee_id()).orElse(null);

        if (!validateDate || tempEmploye == null) {
            response = new Ejerccio04Response(null, false);
            return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
        }
        try {

            horas = employeHoursRepository.coutnHours(req.getEmployee_id(), req.getStart_date(), req.getEnd_date());
        } catch (Exception e) {
            e.printStackTrace();
            response = new Ejerccio04Response(horas, false);
            return new ResponseEntity<Object>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }


        response = new Ejerccio04Response(horas == null ? 0 : horas, true);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> ejerccio05(Ejerccio05Request req) {
        Ejerccio05Response response;
        Integer horas = null;
        boolean validateDate = validations.validarFecha(req.getStart_date(), req.getEnd_date());
        Employees tempEmploye = employeRepository.findById(req.getEmployee_id()).orElse(null);
        if (!validateDate || tempEmploye == null) {
            response = new Ejerccio05Response(null, false);
            return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
        }
        try {

            horas = employeRepository.salaryforHours(req.getEmployee_id(), req.getStart_date(), req.getEnd_date());
        } catch (Exception e) {
            response = new Ejerccio05Response(horas, false);
            return new ResponseEntity<Object>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }


        response = new Ejerccio05Response(horas == null ? 0 : horas, true);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    @Data
    public class Search implements Runnable {
        private Integer idEmpleado;

        private ArrayList<Ejerrcio03EmployeeResponse> list;

        public Search(Integer idEmpleado, ArrayList<Ejerrcio03EmployeeResponse> list) {
            this.idEmpleado = idEmpleado;
            this.list = list;
        }

        @Override
        public void run() {
            System.out.println("Buscando " + idEmpleado);
            Employees empleado = employeRepository.findById(idEmpleado).orElse(null);
            if (empleado != null) {
                Ejerrcio03EmployeeResponse tempral = parserServiceImp.parse(empleado);
                list.add(tempral);
                lista.add(tempral);
            }

        }
    }
}
