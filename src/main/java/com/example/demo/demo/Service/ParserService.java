package com.example.demo.demo.Service;


import com.example.demo.demo.Entity.Employees;
import com.example.demo.demo.Response.Ejerccio02ResponseEmploye;
import com.example.demo.demo.Response.Ejerrcio03EmployeeResponse;

import java.util.List;

public interface ParserService {
    Ejerrcio03EmployeeResponse parse (Employees empelado);
    List<Ejerccio02ResponseEmploye> parseEmplyee(List<Employees> empleados,List<Integer> empleadoss);
}
