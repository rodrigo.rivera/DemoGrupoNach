package com.example.demo.demo.Service.Impl;

import com.example.demo.demo.Entity.Employees;

import com.example.demo.demo.Entity.Jobs;
import com.example.demo.demo.Repository.GendersRepository;
import com.example.demo.demo.Repository.JobsRepository;
import com.example.demo.demo.Response.Ejerccio02ResponseEmploye;
import com.example.demo.demo.Response.Ejerrcio03EmployeeResponse;
import com.example.demo.demo.Service.ParserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ParserServiceImp implements ParserService {

    @Autowired
    private JobsRepository jobsRepository;

    @Autowired
    private GendersRepository gendersRepository;


    @Override
    public Ejerrcio03EmployeeResponse parse(Employees empelado) {
        Ejerrcio03EmployeeResponse e = new Ejerrcio03EmployeeResponse();
        if (empelado != null) {
            e.setGender_id(empelado.getGender().getId());
            e.setJob_id(empelado.getJob().getId());
            e.setName(empelado.getName());
            e.setLast_name(empelado.getLastName());
            e.setBirthdate(empelado.getBrithdate().toString().split(" ")[0]);

        }

        return e;
    }

    @Override
    public List<Ejerccio02ResponseEmploye> parseEmplyee(List<Employees> empleados, List<Integer> empleadoss) {
        List<Ejerccio02ResponseEmploye> lista = new ArrayList<>();
        Jobs jooob = jobsRepository.findById(1).orElse(null);
        for (Employees i : empleados) {

            Jobs joob = jobsRepository.findById(i.getJob().getId()).orElse(null);
            Ejerccio02ResponseEmploye t = new Ejerccio02ResponseEmploye(i.getId(), i.getName(), i.getLastName(), i.getBrithdate(), i.getJob(), null);
            lista.add(t);
        }

        return lista;
    }

}
