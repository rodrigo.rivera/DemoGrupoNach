package com.example.demo.demo.Request;

import lombok.Data;

import java.util.Date;

@Data
public class Ejerccio01Request {

    private Integer gender_id;
    private Integer job_id;
    private String name;
    private String last_name;
    private String birthdate;
    private Date fecha;

}
