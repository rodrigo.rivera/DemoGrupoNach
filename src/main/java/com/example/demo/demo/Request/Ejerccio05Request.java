package com.example.demo.demo.Request;

import lombok.Data;


@Data
public class Ejerccio05Request {

    private Integer employee_id;
    private String start_date;
    private String end_date;

}
