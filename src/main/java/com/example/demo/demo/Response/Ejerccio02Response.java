package com.example.demo.demo.Response;

import lombok.Data;

import java.util.List;

@Data
public class Ejerccio02Response {
    private List<Ejerccio02ResponseEmploye> employees;
    private boolean success;

    public Ejerccio02Response(List<Ejerccio02ResponseEmploye> employees, boolean success) {
        this.employees = employees;
        this.success = success;
    }
}
