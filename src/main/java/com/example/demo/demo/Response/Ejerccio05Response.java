package com.example.demo.demo.Response;

import lombok.Data;

@Data
public class Ejerccio05Response {

    private Integer payment;
    private boolean success;

    public Ejerccio05Response(Integer payment, boolean success) {
        this.payment = payment;
        this.success = success;
    }
}
