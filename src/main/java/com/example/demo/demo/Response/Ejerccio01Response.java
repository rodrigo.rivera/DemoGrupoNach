package com.example.demo.demo.Response;

import lombok.Data;

import java.util.Date;

@Data

public class Ejerccio01Response {

    private Integer id;
    private Boolean success;

    public Ejerccio01Response(Integer id, Boolean success) {
        this.id = id;
        this.success = success;
    }
}
