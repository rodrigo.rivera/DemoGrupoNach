package com.example.demo.demo.Response;

import lombok.Data;

@Data
public class Ejerrcio03EmployeeResponse {

    private Integer gender_id;
    private Integer job_id;
    private String name;
    private String last_name;
    private String birthdate;
}
