package com.example.demo.demo.Response;

import com.example.demo.demo.Entity.Genders;
import com.example.demo.demo.Entity.Jobs;
import lombok.Data;
import org.springframework.boot.autoconfigure.batch.BatchProperties;

import java.util.Date;

@Data
public class Ejerccio02ResponseEmploye {

    private Integer id;
    private String name;
    private String lastName;
    private Date brithdate;
    private Jobs job;
    private Genders gender;


    public Ejerccio02ResponseEmploye(Integer id, String name, String lastName, Date brithdate, Jobs job, Genders gender) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.brithdate = brithdate;
        this.job = job;
        this.gender = gender;
    }
}
