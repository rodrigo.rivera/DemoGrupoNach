package com.example.demo.demo.Response;

import lombok.Data;

@Data
public class Ejerccio04Response {

    private Integer total_worked_hours;
    private boolean success;

    public Ejerccio04Response(Integer total_worked_hours, boolean success) {
        this.total_worked_hours = total_worked_hours;
        this.success = success;
    }
}
