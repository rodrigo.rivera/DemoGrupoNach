package com.example.demo.demo.Controller;

import com.example.demo.demo.Request.*;
import com.example.demo.demo.Service.EjerccioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ejerccio")
public class EjercciosController {

    @Autowired
    private EjerccioService ejerccioService;

    @PostMapping("/example01")
    public ResponseEntity<?> ejerccio01(@RequestBody(required = true) Ejerccio01Request req) {

        ResponseEntity<?> response = ejerccioService.ejerccio01(req);

        return response;
    }

    @GetMapping("/example02")
    public ResponseEntity<?> ejerccio02(@RequestBody(required = true) Ejerccio02Request req) {


        ResponseEntity<?> response = ejerccioService.ejerccio02(req);

        return response;
    }

    @GetMapping("/example03")
    public ResponseEntity<?> ejerccio03(@RequestBody(required = true) Ejerccio03Request req) {


        ResponseEntity<?> response = ejerccioService.ejerccio03(req);

        return response;
    }

    @GetMapping("/example04")
    public ResponseEntity<?> ejerccio04(@RequestBody(required = true) Ejerccio04Request req) {
        ResponseEntity<?> response = ejerccioService.ejerccio04(req);

        return response;
    }


    @GetMapping("/example05")
    public ResponseEntity<?> ejerccio05(@RequestBody(required = true) Ejerccio05Request req) {

        ResponseEntity<?> response = ejerccioService.ejerccio05(req);

        return response;
    }
}
