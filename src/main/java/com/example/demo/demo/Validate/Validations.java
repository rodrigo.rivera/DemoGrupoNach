package com.example.demo.demo.Validate;

import com.example.demo.demo.Request.Ejerccio01Request;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class Validations {



    public boolean validartMayor(String fecha) {
        int mayorEdad = 6570;
        boolean isMayor = false;
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date fechaInicio = sdf.parse(fecha);
            Date fechaactual = new Date(System.currentTimeMillis());
            int milisecondsByDay = 86400000;
            int dias = (int) ((fechaactual.getTime() - fechaInicio.getTime()) / milisecondsByDay);
            if (dias >= mayorEdad) {
                isMayor = true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return isMayor;
    }

    public boolean validarFecha(String fechaI, String fechaF) {
        boolean fechaValida = false;
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date fechaInicio = sdf.parse(fechaI);
            Date fechaFin = sdf.parse(fechaF);

            int dias = (int) ((fechaFin.getTime() - fechaInicio.getTime()) );
            if (dias < 0) {
                fechaValida = true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return fechaValida;
    }

}
