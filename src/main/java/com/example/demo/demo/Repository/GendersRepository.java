package com.example.demo.demo.Repository;


import com.example.demo.demo.Entity.Genders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GendersRepository extends JpaRepository<Genders, Integer> {
}
