package com.example.demo.demo.Repository;


import com.example.demo.demo.Entity.Employees;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeRepository extends JpaRepository<Employees, Integer> {


//{call SearchNameAndLastName(:name, :lastName)}"

    @Query(nativeQuery = true, value = "EXECUTE SearchNameAndLastName :name, :lastName")
    Integer procedureName(@Param("name") String nombre, @Param("lastName") String lastName);

    @Query(nativeQuery = true, value = "EXECUTE countSalaryDate :idEmploye, :fechaI, :fechaF")
    Integer salaryforHours(@Param("idEmploye") Integer idEmploye, @Param("fechaI") String fechaI, @Param("fechaF") String fechaF);

    @Query(nativeQuery = true, value = "select e.id    from EMPLOYEES e")
    List<Integer> getEmpleados(@Param("idJob") Integer idJob);






}
