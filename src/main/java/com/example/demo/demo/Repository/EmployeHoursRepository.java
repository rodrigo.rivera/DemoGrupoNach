package com.example.demo.demo.Repository;


import com.example.demo.demo.Entity.EmployeeEorkedHours;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeHoursRepository extends JpaRepository<EmployeeEorkedHours, Integer> {

    @Query(nativeQuery = true, value = "EXECUTE SearchDate :idEmpleye , :fechaI , :fechaF")
    Integer coutnHours(@Param("idEmpleye") Integer idEmpleye,@Param("fechaI") String fechaI, @Param("fechaF") String fechaF);


}
