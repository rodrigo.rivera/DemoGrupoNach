create database  MyDB;
create schema  dbo;
use MyDB;


CREATE TABLE GENDERS
(
    ID   int IDENTITY (1,1) PRIMARY KEY,
    NAME VARCHAR(256)

);


CREATE TABLE JOBS
(
    ID     int IDENTITY (1,1) PRIMARY KEY,
    NAME   VARCHAR(256),
    SALARY INTEGER

);


CREATE TABLE EMPLOYEES
(
    ID        int IDENTITY (1,1) PRIMARY KEY,
    GENDER_ID INTEGER,
    JOB_ID    INTEGER,
    NAME      VARCHAR(255),
    LAST_NAME VARCHAR(255),
    BRITHDATE DATE,
    constraint fk_EMPLOYEE_JOB FOREIGN KEY (JOB_ID) REFERENCES JOBS (ID),
    constraint fk_EMPLOYEE_GENDER FOREIGN KEY (GENDER_ID) REFERENCES GENDERS (ID),

);


CREATE TABLE EMPLOYEE_EORKED_HOURS
(
    ID           int IDENTITY (1,1) PRIMARY KEY,
    WORKED_HOURS INTEGER,
    WORKED_DATE  DATE,
    EMPLOYEE_ID  INTEGER,
    constraint fk_EMPLOYEE_EORKED_HOURS_EMPLOYEES FOREIGN KEY (EMPLOYEE_ID) REFERENCES EMPLOYEES (ID),
);
---------procesos

CREATE PROCEDURE SearchNameAndLastName @name nvarchar(255),
                                       @lastName nvarchar(255)
AS
SELECT count(*)
FROM EMPLOYEES
WHERE NAME = @name
   or LAST_NAME = @lastName

go


CREATE PROCEDURE empleayeForJob @idJob integer
AS
select e.*
from JOBS b
         inner join EMPLOYEES E on b.ID = E.JOB_ID
where b.ID = @idJob
go


CREATE PROCEDURE SearchDate @idEmploye integer, @fechaI nvarchar(255),
                            @fechaF nvarchar(255)
AS
select sum(e.WORKED_HOURS)
from EMPLOYEE_EORKED_HOURS e
where EMPLOYEE_ID = @idEmploye
  and WORKED_DATE between @fechaI and @fechaF;

go




CREATE PROCEDURE countSalaryDate @idEmploye integer, @fechaI nvarchar(255),
                                 @fechaF nvarchar(255)
AS
select sum(eh.WORKED_HOURS) * (select SALARY
                               from JOBS j
                                        inner join EMPLOYEES E on j.ID = E.JOB_ID
                               where e.ID = @idEmploye)
from EMPLOYEE_EORKED_HOURS eh
         inner join EMPLOYEES E on E.ID = eh.EMPLOYEE_ID
where EMPLOYEE_ID = @idEmploye
  and WORKED_DATE between @fechaI and @fechaF;
go




/*** insert**/

INSERT INTO GENDERS (ID, NAME) VALUES (1, 'Masculino');
INSERT INTO GENDERS (ID, NAME) VALUES (2, 'Femenino');
INSERT INTO GENDERS (ID, NAME) VALUES (3, 'Indefinido');



INSERT INTO JOBS (ID, NAME, SALARY) VALUES (1, 'job1', 10000);
INSERT INTO JOBS (ID, NAME, SALARY) VALUES (2, 'job2', 20000);
INSERT INTO JOBS (ID, NAME, SALARY) VALUES (3, 'job3', 30000);

INSERT INTO EMPLOYEES (ID, GENDER_ID, JOB_ID, NAME, LAST_NAME, BRITHDATE) VALUES (1, 1, 1, 'rodrigo', 'rivera', '1993-07-14');
INSERT INTO EMPLOYEES (ID, GENDER_ID, JOB_ID, NAME, LAST_NAME, BRITHDATE) VALUES (2, 1, 2, 'valeria', 'rivera', '1993-07-14');
INSERT INTO EMPLOYEES (ID, GENDER_ID, JOB_ID, NAME, LAST_NAME, BRITHDATE) VALUES (3, 1, 2, 'Jua', 'Pérez', '1993-07-14');
INSERT INTO EMPLOYEES (ID, GENDER_ID, JOB_ID, NAME, LAST_NAME, BRITHDATE) VALUES (4, 1, 2, 'Juan1', 'Pérez1', '2000-01-01');


INSERT INTO EMPLOYEE_EORKED_HOURS (ID, WORKED_HOURS, WORKED_DATE, EMPLOYEE_ID) VALUES (1, 4, '2022-12-05', 1);
INSERT INTO EMPLOYEE_EORKED_HOURS (ID, WORKED_HOURS, WORKED_DATE, EMPLOYEE_ID) VALUES (2, 5, '2022-12-06', 1);
INSERT INTO EMPLOYEE_EORKED_HOURS (ID, WORKED_HOURS, WORKED_DATE, EMPLOYEE_ID) VALUES (3, 20, '2022-12-04', 1);